﻿#include <iostream>
#include<time.h>
using namespace std;

int main()
{
	const int N = 5;
	int array[N][N]; 

	// создание массива
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			cout << (array[i][j] = i+j);
		}
		cout << "\n";
	}

	// достаем текущую дату
	time_t t = time(0);
	struct tm buf;
	localtime_s (&buf, &t);

	// остаток от деления
	int a = buf.tm_mday % N;

	// суммируем строчку с индексом a
	int summ = 0;
	for (int j = 0; j < N; j++)
	{
		summ = summ + array[a][j];
	}
	cout << "\n" << summ;
}